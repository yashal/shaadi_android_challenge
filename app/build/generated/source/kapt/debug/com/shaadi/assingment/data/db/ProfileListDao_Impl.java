package com.shaadi.assingment.data.db;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.shaadi.assingment.data.network.response.DOBResponse;
import com.shaadi.assingment.data.network.response.LocationResponse;
import com.shaadi.assingment.data.network.response.NameResponse;
import com.shaadi.assingment.data.network.response.PictureResponse;
import com.shaadi.assingment.data.network.response.ProfileResult;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class ProfileListDao_Impl implements ProfileListDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<ProfileResult> __insertionAdapterOfProfileResult;

  private final Converters __converters = new Converters();

  public ProfileListDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfProfileResult = new EntityInsertionAdapter<ProfileResult>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `ProfileResult` (`gender`,`name`,`location`,`email`,`dob`,`registered`,`phone`,`cell`,`nat`,`picture`,`isActionShown`,`action`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProfileResult value) {
        if (value.getGender() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getGender());
        }
        final String _tmp;
        _tmp = __converters.nameResponseToString(value.getName());
        if (_tmp == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, _tmp);
        }
        final String _tmp_1;
        _tmp_1 = __converters.locationResponseToString(value.getLocation());
        if (_tmp_1 == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, _tmp_1);
        }
        if (value.getEmail() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getEmail());
        }
        final String _tmp_2;
        _tmp_2 = __converters.dobResponseToString(value.getDob());
        if (_tmp_2 == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, _tmp_2);
        }
        final String _tmp_3;
        _tmp_3 = __converters.dobResponseToString(value.getRegistered());
        if (_tmp_3 == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, _tmp_3);
        }
        if (value.getPhone() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getPhone());
        }
        if (value.getCell() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getCell());
        }
        if (value.getNat() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getNat());
        }
        final String _tmp_4;
        _tmp_4 = __converters.pictureResponseToString(value.getPicture());
        if (_tmp_4 == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, _tmp_4);
        }
        final int _tmp_5;
        _tmp_5 = value.isActionShown() ? 1 : 0;
        stmt.bindLong(11, _tmp_5);
        if (value.getAction() == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.getAction());
        }
      }
    };
  }

  @Override
  public void saveProfileList(final List<ProfileResult> profileList) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfProfileResult.insert(profileList);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<ProfileResult> getProfileList() {
    final String _sql = "SELECT * FROM ProfileResult";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfGender = CursorUtil.getColumnIndexOrThrow(_cursor, "gender");
      final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
      final int _cursorIndexOfLocation = CursorUtil.getColumnIndexOrThrow(_cursor, "location");
      final int _cursorIndexOfEmail = CursorUtil.getColumnIndexOrThrow(_cursor, "email");
      final int _cursorIndexOfDob = CursorUtil.getColumnIndexOrThrow(_cursor, "dob");
      final int _cursorIndexOfRegistered = CursorUtil.getColumnIndexOrThrow(_cursor, "registered");
      final int _cursorIndexOfPhone = CursorUtil.getColumnIndexOrThrow(_cursor, "phone");
      final int _cursorIndexOfCell = CursorUtil.getColumnIndexOrThrow(_cursor, "cell");
      final int _cursorIndexOfNat = CursorUtil.getColumnIndexOrThrow(_cursor, "nat");
      final int _cursorIndexOfPicture = CursorUtil.getColumnIndexOrThrow(_cursor, "picture");
      final int _cursorIndexOfIsActionShown = CursorUtil.getColumnIndexOrThrow(_cursor, "isActionShown");
      final int _cursorIndexOfAction = CursorUtil.getColumnIndexOrThrow(_cursor, "action");
      final List<ProfileResult> _result = new ArrayList<ProfileResult>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final ProfileResult _item;
        final String _tmpGender;
        _tmpGender = _cursor.getString(_cursorIndexOfGender);
        final NameResponse _tmpName;
        final String _tmp;
        _tmp = _cursor.getString(_cursorIndexOfName);
        _tmpName = __converters.stringToNameResponse(_tmp);
        final LocationResponse _tmpLocation;
        final String _tmp_1;
        _tmp_1 = _cursor.getString(_cursorIndexOfLocation);
        _tmpLocation = __converters.stringToLocationResponse(_tmp_1);
        final String _tmpEmail;
        _tmpEmail = _cursor.getString(_cursorIndexOfEmail);
        final DOBResponse _tmpDob;
        final String _tmp_2;
        _tmp_2 = _cursor.getString(_cursorIndexOfDob);
        _tmpDob = __converters.stringToDOBResponse(_tmp_2);
        final DOBResponse _tmpRegistered;
        final String _tmp_3;
        _tmp_3 = _cursor.getString(_cursorIndexOfRegistered);
        _tmpRegistered = __converters.stringToDOBResponse(_tmp_3);
        final String _tmpPhone;
        _tmpPhone = _cursor.getString(_cursorIndexOfPhone);
        final String _tmpCell;
        _tmpCell = _cursor.getString(_cursorIndexOfCell);
        final String _tmpNat;
        _tmpNat = _cursor.getString(_cursorIndexOfNat);
        final PictureResponse _tmpPicture;
        final String _tmp_4;
        _tmp_4 = _cursor.getString(_cursorIndexOfPicture);
        _tmpPicture = __converters.stringToPictureResponse(_tmp_4);
        final boolean _tmpIsActionShown;
        final int _tmp_5;
        _tmp_5 = _cursor.getInt(_cursorIndexOfIsActionShown);
        _tmpIsActionShown = _tmp_5 != 0;
        final String _tmpAction;
        _tmpAction = _cursor.getString(_cursorIndexOfAction);
        _item = new ProfileResult(_tmpGender,_tmpName,_tmpLocation,_tmpEmail,_tmpDob,_tmpRegistered,_tmpPhone,_tmpCell,_tmpNat,_tmpPicture,_tmpIsActionShown,_tmpAction);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
