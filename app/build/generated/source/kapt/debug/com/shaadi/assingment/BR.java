package com.shaadi.assingment;

public class BR {
  public static final int _all = 0;

  public static final int action = 1;

  public static final int buttonText = 2;

  public static final int buttonVisibility = 3;

  public static final int data = 4;

  public static final int errorModel = 5;

  public static final int errorSubTitle = 6;

  public static final int errorTitle = 7;

  public static final int viewModel = 8;

  public static final int visibility = 9;
}
