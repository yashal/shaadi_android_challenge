package com.shaadi.assingment.databinding;
import com.shaadi.assingment.R;
import com.shaadi.assingment.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LayoutProfileItemBindingImpl extends LayoutProfileItemBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.buttonLayout, 8);
        sViewsWithIds.put(R.id.rejected, 9);
        sViewsWithIds.put(R.id.accepted, 10);
        sViewsWithIds.put(R.id.actionLayout, 11);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final androidx.appcompat.widget.AppCompatImageView mboundView1;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView2;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView3;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView4;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView5;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView6;
    @NonNull
    private final androidx.appcompat.widget.AppCompatTextView mboundView7;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LayoutProfileItemBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 12, sIncludes, sViewsWithIds));
    }
    private LayoutProfileItemBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.AppCompatImageView) bindings[10]
            , (android.widget.LinearLayout) bindings[11]
            , (android.widget.LinearLayout) bindings[8]
            , (androidx.appcompat.widget.AppCompatImageView) bindings[9]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.appcompat.widget.AppCompatImageView) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (androidx.appcompat.widget.AppCompatTextView) bindings[2];
        this.mboundView2.setTag(null);
        this.mboundView3 = (androidx.appcompat.widget.AppCompatTextView) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (androidx.appcompat.widget.AppCompatTextView) bindings[4];
        this.mboundView4.setTag(null);
        this.mboundView5 = (androidx.appcompat.widget.AppCompatTextView) bindings[5];
        this.mboundView5.setTag(null);
        this.mboundView6 = (androidx.appcompat.widget.AppCompatTextView) bindings[6];
        this.mboundView6.setTag(null);
        this.mboundView7 = (androidx.appcompat.widget.AppCompatTextView) bindings[7];
        this.mboundView7.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.data == variableId) {
            setData((com.shaadi.assingment.data.network.response.ProfileResult) variable);
        }
        else if (BR.action == variableId) {
            setAction((java.lang.String) variable);
        }
        else if (BR.viewModel == variableId) {
            setViewModel((com.shaadi.assingment.ui.viewmodel.MainViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setData(@Nullable com.shaadi.assingment.data.network.response.ProfileResult Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }
    public void setAction(@Nullable java.lang.String Action) {
        this.mAction = Action;
    }
    public void setViewModel(@Nullable com.shaadi.assingment.ui.viewmodel.MainViewModel ViewModel) {
        this.mViewModel = ViewModel;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String dataNameTitleJavaLangStringDataNameFirst = null;
        java.lang.String dataNameFirst = null;
        java.lang.String dataGender = null;
        java.lang.String dataNameLast = null;
        java.lang.String dataLocationCountry = null;
        boolean dataGenderEqualsJavaLangStringMale = false;
        java.lang.String dataLocationState = null;
        com.shaadi.assingment.data.network.response.PictureResponse dataPicture = null;
        java.lang.String dataPictureLarge = null;
        com.shaadi.assingment.data.network.response.DOBResponse dataDob = null;
        java.lang.String dataNameTitleJavaLangStringDataNameFirstJavaLangStringDataNameLast = null;
        android.graphics.drawable.Drawable dataGenderEqualsJavaLangStringMaleMboundView3AndroidDrawableIcMaleMboundView3AndroidDrawableIcFemale = null;
        java.lang.String dataLocationCityJavaLangStringDataLocationStateJavaLangString = null;
        java.lang.String dataNameTitle = null;
        com.shaadi.assingment.data.network.response.ProfileResult data = mData;
        java.lang.String dataLocationCity = null;
        java.lang.String dataDobAge = null;
        java.lang.String dataGenderToUpperCase = null;
        java.lang.String dataLocationCityJavaLangStringDataLocationState = null;
        java.lang.String javaLangStringNationalityDataNat = null;
        java.lang.String dataNameTitleJavaLangString = null;
        com.shaadi.assingment.data.network.response.NameResponse dataName = null;
        java.lang.String dataLocationCityJavaLangStringDataLocationStateJavaLangStringDataLocationCountry = null;
        com.shaadi.assingment.data.network.response.LocationResponse dataLocation = null;
        java.lang.String dataNameTitleJavaLangStringDataNameFirstJavaLangString = null;
        java.lang.String dataDobAgeJavaLangStringYears = null;
        java.lang.String dataAction = null;
        java.lang.String dataLocationCityJavaLangString = null;
        java.lang.String dataNat = null;

        if ((dirtyFlags & 0x9L) != 0) {



                if (data != null) {
                    // read data.gender
                    dataGender = data.getGender();
                    // read data.picture
                    dataPicture = data.getPicture();
                    // read data.dob
                    dataDob = data.getDob();
                    // read data.name
                    dataName = data.getName();
                    // read data.location
                    dataLocation = data.getLocation();
                    // read data.action
                    dataAction = data.getAction();
                    // read data.nat
                    dataNat = data.getNat();
                }


                if (dataGender != null) {
                    // read data.gender.equals("male")
                    dataGenderEqualsJavaLangStringMale = dataGender.equals("male");
                    // read data.gender.toUpperCase()
                    dataGenderToUpperCase = dataGender.toUpperCase();
                }
            if((dirtyFlags & 0x9L) != 0) {
                if(dataGenderEqualsJavaLangStringMale) {
                        dirtyFlags |= 0x20L;
                }
                else {
                        dirtyFlags |= 0x10L;
                }
            }
                if (dataPicture != null) {
                    // read data.picture.large
                    dataPictureLarge = dataPicture.getLarge();
                }
                if (dataDob != null) {
                    // read data.dob.age
                    dataDobAge = dataDob.getAge();
                }
                if (dataName != null) {
                    // read data.name.first
                    dataNameFirst = dataName.getFirst();
                    // read data.name.last
                    dataNameLast = dataName.getLast();
                    // read data.name.title
                    dataNameTitle = dataName.getTitle();
                }
                if (dataLocation != null) {
                    // read data.location.country
                    dataLocationCountry = dataLocation.getCountry();
                    // read data.location.state
                    dataLocationState = dataLocation.getState();
                    // read data.location.city
                    dataLocationCity = dataLocation.getCity();
                }
                // read ("Nationality: ") + (data.nat)
                javaLangStringNationalityDataNat = ("Nationality: ") + (dataNat);


                // read data.gender.equals("male") ? @android:drawable/ic_male : @android:drawable/ic_female
                dataGenderEqualsJavaLangStringMaleMboundView3AndroidDrawableIcMaleMboundView3AndroidDrawableIcFemale = ((dataGenderEqualsJavaLangStringMale) ? (getDrawableFromResource(mboundView3, R.drawable.ic_male)) : (getDrawableFromResource(mboundView3, R.drawable.ic_female)));
                // read (data.dob.age) + (" Years")
                dataDobAgeJavaLangStringYears = (dataDobAge) + (" Years");
                // read (data.name.title) + (" ")
                dataNameTitleJavaLangString = (dataNameTitle) + (" ");
                // read (data.location.city) + (", ")
                dataLocationCityJavaLangString = (dataLocationCity) + (", ");


                // read ((data.name.title) + (" ")) + (data.name.first)
                dataNameTitleJavaLangStringDataNameFirst = (dataNameTitleJavaLangString) + (dataNameFirst);
                // read ((data.location.city) + (", ")) + (data.location.state)
                dataLocationCityJavaLangStringDataLocationState = (dataLocationCityJavaLangString) + (dataLocationState);


                // read (((data.name.title) + (" ")) + (data.name.first)) + (" ")
                dataNameTitleJavaLangStringDataNameFirstJavaLangString = (dataNameTitleJavaLangStringDataNameFirst) + (" ");
                // read (((data.location.city) + (", ")) + (data.location.state)) + (", ")
                dataLocationCityJavaLangStringDataLocationStateJavaLangString = (dataLocationCityJavaLangStringDataLocationState) + (", ");


                // read ((((data.name.title) + (" ")) + (data.name.first)) + (" ")) + (data.name.last)
                dataNameTitleJavaLangStringDataNameFirstJavaLangStringDataNameLast = (dataNameTitleJavaLangStringDataNameFirstJavaLangString) + (dataNameLast);
                // read ((((data.location.city) + (", ")) + (data.location.state)) + (", ")) + (data.location.country)
                dataLocationCityJavaLangStringDataLocationStateJavaLangStringDataLocationCountry = (dataLocationCityJavaLangStringDataLocationStateJavaLangString) + (dataLocationCountry);
        }
        // batch finished
        if ((dirtyFlags & 0x9L) != 0) {
            // api target 1

            com.shaadi.assingment.utils.BindingUtil.setImage(this.mboundView1, dataPictureLarge, getDrawableFromResource(mboundView1, R.drawable.ic_image_placeholder_wrapper));
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView2, dataNameTitleJavaLangStringDataNameFirstJavaLangStringDataNameLast);
            androidx.databinding.adapters.TextViewBindingAdapter.setDrawableStart(this.mboundView3, dataGenderEqualsJavaLangStringMaleMboundView3AndroidDrawableIcMaleMboundView3AndroidDrawableIcFemale);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView3, dataGenderToUpperCase);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView4, dataDobAgeJavaLangStringYears);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView5, dataLocationCityJavaLangStringDataLocationStateJavaLangStringDataLocationCountry);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView6, javaLangStringNationalityDataNat);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView7, dataAction);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): data
        flag 1 (0x2L): action
        flag 2 (0x3L): viewModel
        flag 3 (0x4L): null
        flag 4 (0x5L): data.gender.equals("male") ? @android:drawable/ic_male : @android:drawable/ic_female
        flag 5 (0x6L): data.gender.equals("male") ? @android:drawable/ic_male : @android:drawable/ic_female
    flag mapping end*/
    //end
}