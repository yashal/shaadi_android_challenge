package com.shaadi.assingment.data.db;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class AppDataBase_Impl extends AppDataBase {
  private volatile ProfileListDao _profileListDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `ProfileResult` (`gender` TEXT NOT NULL, `name` TEXT NOT NULL, `location` TEXT NOT NULL, `email` TEXT NOT NULL, `dob` TEXT NOT NULL, `registered` TEXT NOT NULL, `phone` TEXT NOT NULL, `cell` TEXT NOT NULL, `nat` TEXT NOT NULL, `picture` TEXT NOT NULL, `isActionShown` INTEGER NOT NULL, `action` TEXT, PRIMARY KEY(`email`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '946166b278dfde82f6cb93044b8ebc69')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `ProfileResult`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsProfileResult = new HashMap<String, TableInfo.Column>(12);
        _columnsProfileResult.put("gender", new TableInfo.Column("gender", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProfileResult.put("name", new TableInfo.Column("name", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProfileResult.put("location", new TableInfo.Column("location", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProfileResult.put("email", new TableInfo.Column("email", "TEXT", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProfileResult.put("dob", new TableInfo.Column("dob", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProfileResult.put("registered", new TableInfo.Column("registered", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProfileResult.put("phone", new TableInfo.Column("phone", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProfileResult.put("cell", new TableInfo.Column("cell", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProfileResult.put("nat", new TableInfo.Column("nat", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProfileResult.put("picture", new TableInfo.Column("picture", "TEXT", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProfileResult.put("isActionShown", new TableInfo.Column("isActionShown", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProfileResult.put("action", new TableInfo.Column("action", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysProfileResult = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesProfileResult = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoProfileResult = new TableInfo("ProfileResult", _columnsProfileResult, _foreignKeysProfileResult, _indicesProfileResult);
        final TableInfo _existingProfileResult = TableInfo.read(_db, "ProfileResult");
        if (! _infoProfileResult.equals(_existingProfileResult)) {
          return new RoomOpenHelper.ValidationResult(false, "ProfileResult(com.shaadi.assingment.data.network.response.ProfileResult).\n"
                  + " Expected:\n" + _infoProfileResult + "\n"
                  + " Found:\n" + _existingProfileResult);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "946166b278dfde82f6cb93044b8ebc69", "af82eb2d0339539ab2a548d33ac08054");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "ProfileResult");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `ProfileResult`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public ProfileListDao getMovieListDao() {
    if (_profileListDao != null) {
      return _profileListDao;
    } else {
      synchronized(this) {
        if(_profileListDao == null) {
          _profileListDao = new ProfileListDao_Impl(this);
        }
        return _profileListDao;
      }
    }
  }
}
