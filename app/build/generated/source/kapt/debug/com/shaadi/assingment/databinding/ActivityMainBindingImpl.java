package com.shaadi.assingment.databinding;
import com.shaadi.assingment.R;
import com.shaadi.assingment.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityMainBindingImpl extends ActivityMainBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(8);
        sIncludes.setIncludes(4, 
            new String[] {"layout_common_error"},
            new int[] {6},
            new int[] {com.shaadi.assingment.R.layout.layout_common_error});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.errorMessage, 7);
    }
    // views
    @NonNull
    private final androidx.coordinatorlayout.widget.CoordinatorLayout mboundView0;
    @NonNull
    private final android.widget.LinearLayout mboundView1;
    @NonNull
    private final android.widget.LinearLayout mboundView3;
    @NonNull
    private final android.widget.LinearLayout mboundView4;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityMainBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private ActivityMainBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 4
            , (androidx.appcompat.widget.AppCompatTextView) bindings[7]
            , (android.widget.FrameLayout) bindings[2]
            , (com.shaadi.assingment.databinding.LayoutCommonErrorBinding) bindings[6]
            , (androidx.recyclerview.widget.RecyclerView) bindings[5]
            );
        this.frameLayout.setTag(null);
        this.mboundView0 = (androidx.coordinatorlayout.widget.CoordinatorLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.LinearLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView3 = (android.widget.LinearLayout) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView4 = (android.widget.LinearLayout) bindings[4];
        this.mboundView4.setTag(null);
        this.movieListRecyclerView.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        layoutNoTicketsCreated.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (layoutNoTicketsCreated.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.errorModel == variableId) {
            setErrorModel((com.shaadi.assingment.ui.viewmodel.ErrorModel) variable);
        }
        else if (BR.viewModel == variableId) {
            setViewModel((com.shaadi.assingment.ui.viewmodel.MainViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setErrorModel(@Nullable com.shaadi.assingment.ui.viewmodel.ErrorModel ErrorModel) {
        updateRegistration(0, ErrorModel);
        this.mErrorModel = ErrorModel;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.errorModel);
        super.requestRebind();
    }
    public void setViewModel(@Nullable com.shaadi.assingment.ui.viewmodel.MainViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        layoutNoTicketsCreated.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeErrorModel((com.shaadi.assingment.ui.viewmodel.ErrorModel) object, fieldId);
            case 1 :
                return onChangeViewModelOnError((androidx.databinding.ObservableBoolean) object, fieldId);
            case 2 :
                return onChangeLayoutNoTicketsCreated((com.shaadi.assingment.databinding.LayoutCommonErrorBinding) object, fieldId);
            case 3 :
                return onChangeViewModelShimmerVisible((androidx.databinding.ObservableField<java.lang.Boolean>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeErrorModel(com.shaadi.assingment.ui.viewmodel.ErrorModel ErrorModel, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelOnError(androidx.databinding.ObservableBoolean ViewModelOnError, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeLayoutNoTicketsCreated(com.shaadi.assingment.databinding.LayoutCommonErrorBinding LayoutNoTicketsCreated, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelShimmerVisible(androidx.databinding.ObservableField<java.lang.Boolean> ViewModelShimmerVisible, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        int viewModelShimmerVisibleViewVISIBLEViewGONE = 0;
        int viewModelOnErrorViewVISIBLEViewGONE = 0;
        int viewModelOnErrorViewGONEViewVISIBLE = 0;
        com.shaadi.assingment.ui.viewmodel.ErrorModel errorModel = mErrorModel;
        java.lang.Boolean viewModelShimmerVisibleGet = null;
        androidx.databinding.ObservableBoolean viewModelOnError = null;
        int viewModelShimmerVisibleViewGONEViewVISIBLE = 0;
        boolean viewModelOnErrorGet = false;
        com.shaadi.assingment.ui.viewmodel.MainViewModel viewModel = mViewModel;
        androidx.databinding.ObservableField<java.lang.Boolean> viewModelShimmerVisible = null;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelShimmerVisibleGet = false;

        if ((dirtyFlags & 0x21L) != 0) {
        }
        if ((dirtyFlags & 0x3aL) != 0) {


            if ((dirtyFlags & 0x32L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.onError
                        viewModelOnError = viewModel.getOnError();
                    }
                    updateRegistration(1, viewModelOnError);


                    if (viewModelOnError != null) {
                        // read viewModel.onError.get()
                        viewModelOnErrorGet = viewModelOnError.get();
                    }
                if((dirtyFlags & 0x32L) != 0) {
                    if(viewModelOnErrorGet) {
                            dirtyFlags |= 0x200L;
                            dirtyFlags |= 0x800L;
                    }
                    else {
                            dirtyFlags |= 0x100L;
                            dirtyFlags |= 0x400L;
                    }
                }


                    // read viewModel.onError.get() ? View.VISIBLE : View.GONE
                    viewModelOnErrorViewVISIBLEViewGONE = ((viewModelOnErrorGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read viewModel.onError.get() ? View.GONE : View.VISIBLE
                    viewModelOnErrorViewGONEViewVISIBLE = ((viewModelOnErrorGet) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
            if ((dirtyFlags & 0x38L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.shimmerVisible
                        viewModelShimmerVisible = viewModel.getShimmerVisible();
                    }
                    updateRegistration(3, viewModelShimmerVisible);


                    if (viewModelShimmerVisible != null) {
                        // read viewModel.shimmerVisible.get()
                        viewModelShimmerVisibleGet = viewModelShimmerVisible.get();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.shimmerVisible.get())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelShimmerVisibleGet = androidx.databinding.ViewDataBinding.safeUnbox(viewModelShimmerVisibleGet);
                if((dirtyFlags & 0x38L) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelShimmerVisibleGet) {
                            dirtyFlags |= 0x80L;
                            dirtyFlags |= 0x2000L;
                    }
                    else {
                            dirtyFlags |= 0x40L;
                            dirtyFlags |= 0x1000L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.shimmerVisible.get()) ? View.VISIBLE : View.GONE
                    viewModelShimmerVisibleViewVISIBLEViewGONE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelShimmerVisibleGet) ? (android.view.View.VISIBLE) : (android.view.View.GONE));
                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.shimmerVisible.get()) ? View.GONE : View.VISIBLE
                    viewModelShimmerVisibleViewGONEViewVISIBLE = ((androidxDatabindingViewDataBindingSafeUnboxViewModelShimmerVisibleGet) ? (android.view.View.GONE) : (android.view.View.VISIBLE));
            }
        }
        // batch finished
        if ((dirtyFlags & 0x38L) != 0) {
            // api target 1

            this.frameLayout.setVisibility(viewModelShimmerVisibleViewVISIBLEViewGONE);
            this.mboundView4.setVisibility(viewModelShimmerVisibleViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 1

            this.layoutNoTicketsCreated.getRoot().setVisibility(viewModelOnErrorViewVISIBLEViewGONE);
            this.movieListRecyclerView.setVisibility(viewModelOnErrorViewGONEViewVISIBLE);
        }
        if ((dirtyFlags & 0x21L) != 0) {
            // api target 1

            this.layoutNoTicketsCreated.setErrorModel(errorModel);
        }
        executeBindingsOn(layoutNoTicketsCreated);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): errorModel
        flag 1 (0x2L): viewModel.onError
        flag 2 (0x3L): layoutNoTicketsCreated
        flag 3 (0x4L): viewModel.shimmerVisible
        flag 4 (0x5L): viewModel
        flag 5 (0x6L): null
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.shimmerVisible.get()) ? View.VISIBLE : View.GONE
        flag 7 (0x8L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.shimmerVisible.get()) ? View.VISIBLE : View.GONE
        flag 8 (0x9L): viewModel.onError.get() ? View.VISIBLE : View.GONE
        flag 9 (0xaL): viewModel.onError.get() ? View.VISIBLE : View.GONE
        flag 10 (0xbL): viewModel.onError.get() ? View.GONE : View.VISIBLE
        flag 11 (0xcL): viewModel.onError.get() ? View.GONE : View.VISIBLE
        flag 12 (0xdL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.shimmerVisible.get()) ? View.GONE : View.VISIBLE
        flag 13 (0xeL): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.shimmerVisible.get()) ? View.GONE : View.VISIBLE
    flag mapping end*/
    //end
}