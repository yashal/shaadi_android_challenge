package com.shaadi.assingment.ui.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u0000 %2\u00020\u0001:\u0001%B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010 \u001a\u00020!J\u0018\u0010\"\u001a\u00020!2\u0006\u0010#\u001a\u00020\u00112\u0006\u0010$\u001a\u00020\u0011H\u0002R\u001b\u0010\u0005\u001a\u00020\u00068FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u000b\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0017\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0017\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u0010\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0013R\u0017\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00150\u0010\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001aX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001f\u00a8\u0006&"}, d2 = {"Lcom/shaadi/assingment/ui/viewmodel/MainViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lcom/shaadi/assingment/data/repository/MainRepository;", "(Lcom/shaadi/assingment/data/repository/MainRepository;)V", "errorModel", "Lcom/shaadi/assingment/ui/viewmodel/ErrorModel;", "getErrorModel", "()Lcom/shaadi/assingment/ui/viewmodel/ErrorModel;", "errorModel$delegate", "Lkotlin/Lazy;", "onError", "Landroidx/databinding/ObservableBoolean;", "getOnError", "()Landroidx/databinding/ObservableBoolean;", "onFailure", "Landroidx/lifecycle/MutableLiveData;", "", "getOnFailure", "()Landroidx/lifecycle/MutableLiveData;", "onStart", "", "getOnStart", "onSuccess", "getOnSuccess", "shimmerVisible", "Landroidx/databinding/ObservableField;", "", "getShimmerVisible", "()Landroidx/databinding/ObservableField;", "setShimmerVisible", "(Landroidx/databinding/ObservableField;)V", "apiCallToGetMovieList", "", "showErrorModel", "title", "subTitle", "Companion", "app_debug"})
public final class MainViewModel extends androidx.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Object> onStart = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Object> onSuccess = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.String> onFailure = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.databinding.ObservableField<java.lang.Boolean> shimmerVisible;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy errorModel$delegate = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.databinding.ObservableBoolean onError = null;
    private final com.shaadi.assingment.data.repository.MainRepository repository = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ERROR_TITLE = "Something went wrong..";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ERROR_DESCRIPTION = "An alien is probably blocking your signal.";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RETRY = "RETRY";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PROFILE_ERROR_TITLE = "Profile list issue";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PROFILE_ERROR_DESCRIPTION = "No profile available";
    public static final com.shaadi.assingment.ui.viewmodel.MainViewModel.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Object> getOnStart() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Object> getOnSuccess() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getOnFailure() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.databinding.ObservableField<java.lang.Boolean> getShimmerVisible() {
        return null;
    }
    
    public final void setShimmerVisible(@org.jetbrains.annotations.NotNull()
    androidx.databinding.ObservableField<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shaadi.assingment.ui.viewmodel.ErrorModel getErrorModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.databinding.ObservableBoolean getOnError() {
        return null;
    }
    
    public final void apiCallToGetMovieList() {
    }
    
    private final void showErrorModel(java.lang.String title, java.lang.String subTitle) {
    }
    
    public MainViewModel(@org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.data.repository.MainRepository repository) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/shaadi/assingment/ui/viewmodel/MainViewModel$Companion;", "", "()V", "ERROR_DESCRIPTION", "", "ERROR_TITLE", "PROFILE_ERROR_DESCRIPTION", "PROFILE_ERROR_TITLE", "RETRY", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}