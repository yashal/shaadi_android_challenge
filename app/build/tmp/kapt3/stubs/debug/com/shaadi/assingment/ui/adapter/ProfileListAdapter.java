package com.shaadi.assingment.ui.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001cB-\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\b\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\b\u0010\u0012\u001a\u00020\u0013H\u0016J\u0018\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u0013H\u0016J\u0018\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0013H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011\u00a8\u0006\u001d"}, d2 = {"Lcom/shaadi/assingment/ui/adapter/ProfileListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/shaadi/assingment/ui/adapter/ProfileListAdapter$ProfileListAdapterViewHolder;", "context", "Landroid/content/Context;", "movieList", "Ljava/util/ArrayList;", "Lcom/shaadi/assingment/data/network/response/ProfileResult;", "Lkotlin/collections/ArrayList;", "model", "Lcom/shaadi/assingment/ui/viewmodel/MainViewModel;", "(Landroid/content/Context;Ljava/util/ArrayList;Lcom/shaadi/assingment/ui/viewmodel/MainViewModel;)V", "layoutInflater", "Landroid/view/LayoutInflater;", "getMovieList", "()Ljava/util/ArrayList;", "setMovieList", "(Ljava/util/ArrayList;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "ProfileListAdapterViewHolder", "app_debug"})
public final class ProfileListAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.shaadi.assingment.ui.adapter.ProfileListAdapter.ProfileListAdapterViewHolder> {
    private android.view.LayoutInflater layoutInflater;
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<com.shaadi.assingment.data.network.response.ProfileResult> movieList;
    private final com.shaadi.assingment.ui.viewmodel.MainViewModel model = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.shaadi.assingment.ui.adapter.ProfileListAdapter.ProfileListAdapterViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.ui.adapter.ProfileListAdapter.ProfileListAdapterViewHolder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.shaadi.assingment.data.network.response.ProfileResult> getMovieList() {
        return null;
    }
    
    public final void setMovieList(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.shaadi.assingment.data.network.response.ProfileResult> p0) {
    }
    
    public ProfileListAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.shaadi.assingment.data.network.response.ProfileResult> movieList, @org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.ui.viewmodel.MainViewModel model) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0013\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000eR\u0011\u0010\u0015\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\n\u00a8\u0006\u0017"}, d2 = {"Lcom/shaadi/assingment/ui/adapter/ProfileListAdapter$ProfileListAdapterViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "binding", "Lcom/shaadi/assingment/databinding/LayoutProfileItemBinding;", "(Landroid/view/View;Lcom/shaadi/assingment/databinding/LayoutProfileItemBinding;)V", "accepted", "Landroid/widget/ImageView;", "getAccepted", "()Landroid/widget/ImageView;", "actionLayout", "Landroid/widget/LinearLayout;", "getActionLayout", "()Landroid/widget/LinearLayout;", "getBinding", "()Lcom/shaadi/assingment/databinding/LayoutProfileItemBinding;", "setBinding", "(Lcom/shaadi/assingment/databinding/LayoutProfileItemBinding;)V", "buttonLayout", "getButtonLayout", "rejected", "getRejected", "app_debug"})
    public static final class ProfileListAdapterViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final android.widget.LinearLayout buttonLayout = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.LinearLayout actionLayout = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.ImageView rejected = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.ImageView accepted = null;
        @org.jetbrains.annotations.NotNull()
        private com.shaadi.assingment.databinding.LayoutProfileItemBinding binding;
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.LinearLayout getButtonLayout() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.LinearLayout getActionLayout() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageView getRejected() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageView getAccepted() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.shaadi.assingment.databinding.LayoutProfileItemBinding getBinding() {
            return null;
        }
        
        public final void setBinding(@org.jetbrains.annotations.NotNull()
        com.shaadi.assingment.databinding.LayoutProfileItemBinding p0) {
        }
        
        public ProfileListAdapterViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView, @org.jetbrains.annotations.NotNull()
        com.shaadi.assingment.databinding.LayoutProfileItemBinding binding) {
            super(null);
        }
    }
}