package com.shaadi.assingment.data.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0011\u0010\u000b\u001a\u00020\fH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ\u0017\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ\u0016\u0010\u000f\u001a\u00020\u00102\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0013"}, d2 = {"Lcom/shaadi/assingment/data/repository/MainRepository;", "Lcom/shaadi/assingment/data/network/SafeApiRequest;", "api", "Lcom/shaadi/assingment/data/network/RestApiClient;", "db", "Lcom/shaadi/assingment/data/db/AppDataBase;", "(Lcom/shaadi/assingment/data/network/RestApiClient;Lcom/shaadi/assingment/data/db/AppDataBase;)V", "quotes", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/shaadi/assingment/data/network/response/ProfileResult;", "getProfileResult", "Lcom/shaadi/assingment/data/network/response/ProfileResponse;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getReposFromDataBase", "saveReposInDataBase", "", "arrList", "Companion", "app_debug"})
public final class MainRepository extends com.shaadi.assingment.data.network.SafeApiRequest {
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.shaadi.assingment.data.network.response.ProfileResult>> quotes = null;
    private final com.shaadi.assingment.data.network.RestApiClient api = null;
    private final com.shaadi.assingment.data.db.AppDataBase db = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RESULT = "10";
    public static final com.shaadi.assingment.data.repository.MainRepository.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getProfileResult(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.shaadi.assingment.data.network.response.ProfileResponse> p0) {
        return null;
    }
    
    private final void saveReposInDataBase(java.util.List<com.shaadi.assingment.data.network.response.ProfileResult> arrList) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getReposFromDataBase(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<com.shaadi.assingment.data.network.response.ProfileResult>> p0) {
        return null;
    }
    
    public MainRepository(@org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.data.network.RestApiClient api, @org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.data.db.AppDataBase db) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/shaadi/assingment/data/repository/MainRepository$Companion;", "", "()V", "RESULT", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}