package com.shaadi.assingment.data.db;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007J\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00042\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0007J\u0014\u0010\n\u001a\u0004\u0018\u00010\u00042\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0007J\u0014\u0010\r\u001a\u0004\u0018\u00010\u00042\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0007J\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00062\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004H\u0007J\u0014\u0010\u0012\u001a\u0004\u0018\u00010\t2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004H\u0007J\u0014\u0010\u0013\u001a\u0004\u0018\u00010\f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004H\u0007J\u0014\u0010\u0014\u001a\u0004\u0018\u00010\u000f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004H\u0007\u00a8\u0006\u0015"}, d2 = {"Lcom/shaadi/assingment/data/db/Converters;", "", "()V", "dobResponseToString", "", "dobResponse", "Lcom/shaadi/assingment/data/network/response/DOBResponse;", "locationResponseToString", "locationResponse", "Lcom/shaadi/assingment/data/network/response/LocationResponse;", "nameResponseToString", "nameResponse", "Lcom/shaadi/assingment/data/network/response/NameResponse;", "pictureResponseToString", "pictureResponse", "Lcom/shaadi/assingment/data/network/response/PictureResponse;", "stringToDOBResponse", "data", "stringToLocationResponse", "stringToNameResponse", "stringToPictureResponse", "app_debug"})
public final class Converters {
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final com.shaadi.assingment.data.network.response.NameResponse stringToNameResponse(@org.jetbrains.annotations.Nullable()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String nameResponseToString(@org.jetbrains.annotations.Nullable()
    com.shaadi.assingment.data.network.response.NameResponse nameResponse) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final com.shaadi.assingment.data.network.response.LocationResponse stringToLocationResponse(@org.jetbrains.annotations.Nullable()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String locationResponseToString(@org.jetbrains.annotations.Nullable()
    com.shaadi.assingment.data.network.response.LocationResponse locationResponse) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final com.shaadi.assingment.data.network.response.DOBResponse stringToDOBResponse(@org.jetbrains.annotations.Nullable()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String dobResponseToString(@org.jetbrains.annotations.Nullable()
    com.shaadi.assingment.data.network.response.DOBResponse dobResponse) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final com.shaadi.assingment.data.network.response.PictureResponse stringToPictureResponse(@org.jetbrains.annotations.Nullable()
    java.lang.String data) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.TypeConverter()
    public final java.lang.String pictureResponseToString(@org.jetbrains.annotations.Nullable()
    com.shaadi.assingment.data.network.response.PictureResponse pictureResponse) {
        return null;
    }
    
    public Converters() {
        super();
    }
}