package com.shaadi.assingment.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001c\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a \u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0001\u001a\u0006\u0010\u0005\u001a\u00020\u0006\u001a\u0010\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u000b"}, d2 = {"changeDateFormat", "", "currentFormat", "requiredFormat", "dateString", "getCurrentTime", "", "getSimpleDateFormat", "Ljava/text/SimpleDateFormat;", "dateFormat", "Lcom/shaadi/assingment/utils/DateFormat;", "app_debug"})
public final class ViewUtilsKt {
    
    @org.jetbrains.annotations.Nullable()
    public static final java.text.SimpleDateFormat getSimpleDateFormat(@org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.utils.DateFormat dateFormat) {
        return null;
    }
    
    public static final long getCurrentTime() {
        return 0L;
    }
    
    @org.jetbrains.annotations.Nullable()
    public static final java.lang.String changeDateFormat(@org.jetbrains.annotations.NotNull()
    java.lang.String currentFormat, @org.jetbrains.annotations.NotNull()
    java.lang.String requiredFormat, @org.jetbrains.annotations.NotNull()
    java.lang.String dateString) {
        return null;
    }
}