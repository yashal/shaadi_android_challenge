package com.shaadi.assingment.data.network.response;

import java.lang.System;

@androidx.room.Entity()
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\'\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001Bk\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\n\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u0006\u0010\u000e\u001a\u00020\u0003\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0012\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0014J\t\u0010*\u001a\u00020\u0003H\u00c6\u0003J\t\u0010+\u001a\u00020\u0010H\u00c6\u0003J\t\u0010,\u001a\u00020\u0012H\u00c6\u0003J\u000b\u0010-\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010.\u001a\u00020\u0005H\u00c6\u0003J\t\u0010/\u001a\u00020\u0007H\u00c6\u0003J\t\u00100\u001a\u00020\u0003H\u00c6\u0003J\t\u00101\u001a\u00020\nH\u00c6\u0003J\t\u00102\u001a\u00020\nH\u00c6\u0003J\t\u00103\u001a\u00020\u0003H\u00c6\u0003J\t\u00104\u001a\u00020\u0003H\u00c6\u0003J\t\u00105\u001a\u00020\u0003H\u00c6\u0003J\u0083\u0001\u00106\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\n2\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u00032\b\b\u0002\u0010\u000e\u001a\u00020\u00032\b\b\u0002\u0010\u000f\u001a\u00020\u00102\b\b\u0002\u0010\u0011\u001a\u00020\u00122\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u00107\u001a\u00020\u00122\b\u00108\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00109\u001a\u00020:H\u00d6\u0001J\t\u0010;\u001a\u00020\u0003H\u00d6\u0001R\u001c\u0010\u0013\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u0011\u0010\r\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0016R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0016\u0010\b\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0016R\u001a\u0010\u0011\u001a\u00020\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u001e\"\u0004\b\u001f\u0010 R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010$R\u0011\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010\u0016R\u0011\u0010\f\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\u0016R\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\b\n\u0000\u001a\u0004\b\'\u0010(R\u0011\u0010\u000b\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010\u001b\u00a8\u0006<"}, d2 = {"Lcom/shaadi/assingment/data/network/response/ProfileResult;", "", "gender", "", "name", "Lcom/shaadi/assingment/data/network/response/NameResponse;", "location", "Lcom/shaadi/assingment/data/network/response/LocationResponse;", "email", "dob", "Lcom/shaadi/assingment/data/network/response/DOBResponse;", "registered", "phone", "cell", "nat", "picture", "Lcom/shaadi/assingment/data/network/response/PictureResponse;", "isActionShown", "", "action", "(Ljava/lang/String;Lcom/shaadi/assingment/data/network/response/NameResponse;Lcom/shaadi/assingment/data/network/response/LocationResponse;Ljava/lang/String;Lcom/shaadi/assingment/data/network/response/DOBResponse;Lcom/shaadi/assingment/data/network/response/DOBResponse;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/shaadi/assingment/data/network/response/PictureResponse;ZLjava/lang/String;)V", "getAction", "()Ljava/lang/String;", "setAction", "(Ljava/lang/String;)V", "getCell", "getDob", "()Lcom/shaadi/assingment/data/network/response/DOBResponse;", "getEmail", "getGender", "()Z", "setActionShown", "(Z)V", "getLocation", "()Lcom/shaadi/assingment/data/network/response/LocationResponse;", "getName", "()Lcom/shaadi/assingment/data/network/response/NameResponse;", "getNat", "getPhone", "getPicture", "()Lcom/shaadi/assingment/data/network/response/PictureResponse;", "getRegistered", "component1", "component10", "component11", "component12", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "", "toString", "app_debug"})
public final class ProfileResult {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String gender = null;
    @org.jetbrains.annotations.NotNull()
    private final com.shaadi.assingment.data.network.response.NameResponse name = null;
    @org.jetbrains.annotations.NotNull()
    private final com.shaadi.assingment.data.network.response.LocationResponse location = null;
    @org.jetbrains.annotations.NotNull()
    @androidx.room.PrimaryKey(autoGenerate = false)
    private final java.lang.String email = null;
    @org.jetbrains.annotations.NotNull()
    private final com.shaadi.assingment.data.network.response.DOBResponse dob = null;
    @org.jetbrains.annotations.NotNull()
    private final com.shaadi.assingment.data.network.response.DOBResponse registered = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String phone = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String cell = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String nat = null;
    @org.jetbrains.annotations.NotNull()
    private final com.shaadi.assingment.data.network.response.PictureResponse picture = null;
    private boolean isActionShown;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String action;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getGender() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shaadi.assingment.data.network.response.NameResponse getName() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shaadi.assingment.data.network.response.LocationResponse getLocation() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getEmail() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shaadi.assingment.data.network.response.DOBResponse getDob() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shaadi.assingment.data.network.response.DOBResponse getRegistered() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPhone() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCell() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getNat() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shaadi.assingment.data.network.response.PictureResponse getPicture() {
        return null;
    }
    
    public final boolean isActionShown() {
        return false;
    }
    
    public final void setActionShown(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAction() {
        return null;
    }
    
    public final void setAction(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public ProfileResult(@org.jetbrains.annotations.NotNull()
    java.lang.String gender, @org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.data.network.response.NameResponse name, @org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.data.network.response.LocationResponse location, @org.jetbrains.annotations.NotNull()
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.data.network.response.DOBResponse dob, @org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.data.network.response.DOBResponse registered, @org.jetbrains.annotations.NotNull()
    java.lang.String phone, @org.jetbrains.annotations.NotNull()
    java.lang.String cell, @org.jetbrains.annotations.NotNull()
    java.lang.String nat, @org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.data.network.response.PictureResponse picture, boolean isActionShown, @org.jetbrains.annotations.Nullable()
    java.lang.String action) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shaadi.assingment.data.network.response.NameResponse component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shaadi.assingment.data.network.response.LocationResponse component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shaadi.assingment.data.network.response.DOBResponse component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shaadi.assingment.data.network.response.DOBResponse component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component9() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shaadi.assingment.data.network.response.PictureResponse component10() {
        return null;
    }
    
    public final boolean component11() {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component12() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.shaadi.assingment.data.network.response.ProfileResult copy(@org.jetbrains.annotations.NotNull()
    java.lang.String gender, @org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.data.network.response.NameResponse name, @org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.data.network.response.LocationResponse location, @org.jetbrains.annotations.NotNull()
    java.lang.String email, @org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.data.network.response.DOBResponse dob, @org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.data.network.response.DOBResponse registered, @org.jetbrains.annotations.NotNull()
    java.lang.String phone, @org.jetbrains.annotations.NotNull()
    java.lang.String cell, @org.jetbrains.annotations.NotNull()
    java.lang.String nat, @org.jetbrains.annotations.NotNull()
    com.shaadi.assingment.data.network.response.PictureResponse picture, boolean isActionShown, @org.jetbrains.annotations.Nullable()
    java.lang.String action) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}