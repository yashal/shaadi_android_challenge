package com.shaadi.assingment.data.network;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\bf\u0018\u0000 \b2\u00020\u0001:\u0001\bJ!\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0007\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\t"}, d2 = {"Lcom/shaadi/assingment/data/network/RestApiClient;", "", "fetchProfile", "Lretrofit2/Response;", "Lcom/shaadi/assingment/data/network/response/ProfileResponse;", "results", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Companion", "app_debug"})
public abstract interface RestApiClient {
    public static final com.shaadi.assingment.data.network.RestApiClient.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "/api/")
    public abstract java.lang.Object fetchProfile(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "results")
    java.lang.String results, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.shaadi.assingment.data.network.response.ProfileResponse>> p1);
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0086\u0002\u00a8\u0006\u0007"}, d2 = {"Lcom/shaadi/assingment/data/network/RestApiClient$Companion;", "", "()V", "invoke", "Lcom/shaadi/assingment/data/network/RestApiClient;", "networkConnectionInterceptor", "Lcom/shaadi/assingment/data/network/NetworkConnectionInterceptor;", "app_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.shaadi.assingment.data.network.RestApiClient invoke(@org.jetbrains.annotations.NotNull()
        com.shaadi.assingment.data.network.NetworkConnectionInterceptor networkConnectionInterceptor) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}