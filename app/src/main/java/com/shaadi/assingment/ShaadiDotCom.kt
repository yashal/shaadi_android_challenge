package com.shaadi.assingment

import android.app.Application
import com.shaadi.assingment.data.db.AppDataBase
import com.shaadi.assingment.data.network.NetworkConnectionInterceptor
import com.shaadi.assingment.data.network.RestApiClient
import com.shaadi.assingment.data.repository.MainRepository
import com.shaadi.assingment.ui.modelfactory.MainViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class ShaadiDotCom : Application(), KodeinAware{

    override val kodein = Kodein.lazy {
        import(androidXModule(this@ShaadiDotCom))
        bind() from singleton { RestApiClient(instance()) }
        bind() from singleton { AppDataBase(instance()) }
        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { MainRepository(instance(), instance()) }
        bind() from provider { MainViewModelFactory(instance()) }
    }
}