package com.shaadi.assingment.ui.viewmodel


interface ErrorActionListener {
    abstract fun onErrorActionClicked()
}