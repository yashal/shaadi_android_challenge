package com.shaadi.assingment.ui.activity

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.shaadi.assingment.R
import com.shaadi.assingment.data.network.response.ProfileResult
import com.shaadi.assingment.databinding.ActivityMainBinding
import com.shaadi.assingment.ui.adapter.ProfileListAdapter
import com.shaadi.assingment.ui.modelfactory.MainViewModelFactory
import com.shaadi.assingment.ui.viewmodel.MainViewModel
import com.shaadi.assingment.ui.viewmodel.MainViewModel.Companion.PROFILE_ERROR_DESCRIPTION
import com.shaadi.assingment.utils.Coroutines
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class MainActivity : BaseActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory by instance<MainViewModelFactory>()
    private lateinit var mainViewModel: MainViewModel
    private var movieList = ArrayList<ProfileResult>()
    private var mLayoutManager: LinearLayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

        mainViewModel =
            ViewModelProvider(this, factory).get(MainViewModel::class.java)

        binding.viewModel = mainViewModel

        setToolBar(false, "Matches")

        mainViewModel.onFailure.observe(this, Observer {
            Coroutines.main {
                if (it == PROFILE_ERROR_DESCRIPTION) {
                    movieListRecyclerView.visibility = View.GONE
                    errorMessage.visibility = View.VISIBLE
                } else {
                    binding.errorModel = mainViewModel.errorModel
                }
            }
        })

        makeAPICall()

        mainViewModel.onStart.observe(this, Observer {
            errorMessage.visibility = View.GONE
            mainViewModel.shimmerVisible.set(true)
//            mainViewModel.onError.set(false)
        })

        mainViewModel.onSuccess.observe(this, Observer {
            mainViewModel.onError.set(false)
            if (it is ArrayList<*>){
                val response = it as ArrayList<ProfileResult>
                movieList.clear()
                movieList.addAll(response)
                initRecyclerView()
            }
        })
    }

    private fun makeAPICall() {
        mainViewModel.apiCallToGetProfileList()
    }

    private fun initRecyclerView() {
        movieListRecyclerView.apply {
            mLayoutManager = LinearLayoutManager(context)
            layoutManager = mLayoutManager
            adapter = ProfileListAdapter(context, movieList, mainViewModel)
        }
    }

}
