package com.shaadi.assingment.ui.viewmodel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.ViewModel
import com.shaadi.assingment.data.network.response.ProfileResult
import com.shaadi.assingment.data.repository.MainRepository
import com.shaadi.assingment.utils.ApiException
import com.shaadi.assingment.utils.NoInternetException
import kotlinx.coroutines.launch

class MainViewModel(
    private val repository: MainRepository
) : ViewModel() {

    // variable Declaration
    val onStart = MutableLiveData<Any>()
    val onSuccess = MutableLiveData<Any>()
    val onFailure = MutableLiveData<String>()
    var shimmerVisible = ObservableField<Boolean>()
    val errorModel: ErrorModel by lazy { ErrorModel() }
    val onError = ObservableBoolean(false)

    // API call
    fun apiCallToGetProfileList() {

        viewModelScope.launch {
            val list = ArrayList<ProfileResult>()
            list.addAll(repository.getReposFromDataBase())

            if (list.isNotEmpty()) {
                onError.set(false)
                onSuccess.postValue(list)
            } else {

                onStart.postValue("")
                try {
                    val response = repository.getProfileResult()
                    response.let {
                        onError.set(true)
                        shimmerVisible.set(false)
                        if (response.results.size > 0) {
                            onError.set(false)
                            onSuccess.postValue(response.results)
                        }else {
                            onFailure.postValue(PROFILE_ERROR_DESCRIPTION)
                        }
                        return@launch
                    }
                } catch (ex: ApiException) {
                    shimmerVisible.set(false)
                    onError.set(true)
                    showErrorModel(
                        PROFILE_ERROR_TITLE,
                        ERROR_DESCRIPTION
                    )
                    onFailure.postValue("${ex.message}")
                    ex.printStackTrace()
                } catch (ex: NoInternetException) {
                    shimmerVisible.set(false)
                    onError.set(true)
                    showErrorModel(
                        ERROR_TITLE,
                        ex.message!!
                    )
                    onFailure.postValue("${ex.message}")
                    ex.printStackTrace()
                }
            }
        }
    }

    // Show Error Model
    private fun showErrorModel(
        title: String,
        subTitle: String
    ) {
        errorModel.apply {
            errorTitle = title
            errorSubTitle = subTitle
            buttonText = RETRY
            errorActionListener = object : ErrorActionListener {
                override fun onErrorActionClicked() {
                    apiCallToGetProfileList()
                }
            }
        }
    }

    fun updateDB(emailAddress: String, isAction: Boolean, actionValue : String){
        viewModelScope.launch {
            repository.updateValuesInDatabase(emailAddress, isAction, actionValue)
        }
    }

    companion object {
        const val ERROR_TITLE = "Something went wrong.."
        const val ERROR_DESCRIPTION = "An alien is probably blocking your signal."
        const val RETRY = "RETRY"
        const val PROFILE_ERROR_TITLE = "Profile list issue"
        const val PROFILE_ERROR_DESCRIPTION = "No profile available"
    }
}