package com.shaadi.assingment.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.shaadi.assingment.R
import com.shaadi.assingment.data.network.response.ProfileResult
import com.shaadi.assingment.databinding.LayoutProfileItemBinding
import com.shaadi.assingment.ui.viewmodel.MainViewModel


class ProfileListAdapter(
    private val context: Context,
    var profileResult: ArrayList<ProfileResult>,
    private val model: MainViewModel
) : RecyclerView.Adapter<ProfileListAdapter.ProfileListAdapterViewHolder>() {

    private lateinit var layoutInflater: LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileListAdapterViewHolder {

        layoutInflater = LayoutInflater.from(parent.context)
        val binding: LayoutProfileItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.layout_profile_item, parent, false)
        return ProfileListAdapterViewHolder(binding.root, binding)
    }

    override fun getItemCount() = profileResult.size

    override fun onBindViewHolder(holder: ProfileListAdapterViewHolder, position: Int) {
        val profileResult: ProfileResult = profileResult[position]
        holder.binding.data = profileResult
        holder.binding.viewModel = model
        if (profileResult.isActionShown){
            holder.buttonLayout.visibility = View.GONE
            holder.actionLayout.visibility = View.VISIBLE
        } else {
            holder.buttonLayout.visibility = View.VISIBLE
            holder.actionLayout.visibility = View.GONE
        }

        holder.accepted.setOnClickListener {
            if(!profileResult.isActionShown) {
                profileResult.isActionShown = !profileResult.isActionShown
            }
            profileResult.action = "Member Accepted"
            model.updateDB(profileResult.email, profileResult.isActionShown, profileResult.action!!)
            notifyItemChanged(holder.adapterPosition)
        }
        holder.rejected.setOnClickListener {
            if(!profileResult.isActionShown) {
                profileResult.isActionShown = !profileResult.isActionShown
            }
            profileResult.action = "Member Declined"
            model.updateDB(profileResult.email, profileResult.isActionShown, profileResult.action!!)
            notifyItemChanged(holder.adapterPosition)
        }
    }

    class ProfileListAdapterViewHolder(itemView: View, var binding: LayoutProfileItemBinding) :
        RecyclerView.ViewHolder(itemView) {
        val buttonLayout: LinearLayout = itemView.findViewById(R.id.buttonLayout)
        val actionLayout: LinearLayout = itemView.findViewById(R.id.actionLayout)
        val rejected: ImageView = itemView.findViewById(R.id.rejected)
        val accepted: ImageView = itemView.findViewById(R.id.accepted)
    }
}