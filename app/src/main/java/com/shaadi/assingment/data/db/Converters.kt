package com.shaadi.assingment.data.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.shaadi.assingment.data.network.response.DOBResponse
import com.shaadi.assingment.data.network.response.LocationResponse
import com.shaadi.assingment.data.network.response.NameResponse
import com.shaadi.assingment.data.network.response.PictureResponse
import java.lang.reflect.Type


class Converters {

    @TypeConverter
    fun stringToNameResponse(data: String?):
            NameResponse? {

        val objType: Type =
            object : TypeToken<NameResponse?>() {}.type
        return Gson().fromJson(data, objType)
    }

    @TypeConverter
    fun nameResponseToString(nameResponse: NameResponse?): String? {
        return Gson().toJson(nameResponse)
    }

    @TypeConverter
    fun stringToLocationResponse(data: String?):
            LocationResponse? {

        val objType: Type =
            object : TypeToken<LocationResponse?>() {}.type
        return Gson().fromJson(data, objType)
    }

    @TypeConverter
    fun locationResponseToString(locationResponse: LocationResponse?): String? {
        return Gson().toJson(locationResponse)
    }

    @TypeConverter
    fun stringToDOBResponse(data: String?):
            DOBResponse? {

        val objType: Type =
            object : TypeToken<DOBResponse?>() {}.type
        return Gson().fromJson(data, objType)
    }

    @TypeConverter
    fun dobResponseToString(dobResponse: DOBResponse?): String? {
        return Gson().toJson(dobResponse)
    }

    @TypeConverter
    fun stringToPictureResponse(data: String?):
            PictureResponse? {

        val objType: Type =
            object : TypeToken<PictureResponse?>() {}.type
        return Gson().fromJson(data, objType)
    }

    @TypeConverter
    fun pictureResponseToString(pictureResponse: PictureResponse?): String? {
        return Gson().toJson(pictureResponse)
    }
}