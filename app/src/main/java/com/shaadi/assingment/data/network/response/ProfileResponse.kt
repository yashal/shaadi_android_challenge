package com.shaadi.assingment.data.network.response

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey


data class ProfileResponse(
    val results: ArrayList<ProfileResult>
)

@Entity
data class ProfileResult(
    val gender: String,
    val name: NameResponse,
    val location: LocationResponse,
    @PrimaryKey(autoGenerate = false)
    val email: String,
    val dob: DOBResponse,
    val registered: DOBResponse,
    val phone: String,
    val cell: String,
    val nat: String,
    val picture: PictureResponse,
    var isActionShown: Boolean = false,
    var action: String? = ""
)

data class NameResponse(
    val title: String,
    val first: String,
    val last: String
)

data class LocationResponse(
    val street: StreetResponse,
    val city: String,
    val state: String,
    val country: String,
    val postcode: String,
    val coordinates: CoordinatesResponse,
    val timezone: TimezoneResponse
)

data class StreetResponse(
    val number: Long,
    val name: String
)

data class CoordinatesResponse(
    val latitude: String,
    val longitude: String
)

data class TimezoneResponse(
    val offset: String,
    val description: String
)

data class DOBResponse(
    val date: String,
    val age: String
)

data class PictureResponse(
    val large: String,
    val medium: String,
    val thumbnail: String
)