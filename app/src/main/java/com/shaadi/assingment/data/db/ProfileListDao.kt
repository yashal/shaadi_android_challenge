package com.shaadi.assingment.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.shaadi.assingment.data.network.response.ProfileResult

@Dao
interface ProfileListDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveProfileList(profileList: List<ProfileResult>)

    @Query("SELECT * FROM ProfileResult")
    fun getProfileList(): List<ProfileResult>

    @Query("UPDATE ProfileResult SET isActionShown = :isAction, `action` = :actionValue WHERE email = :emailAddress")
    fun updateProfile(emailAddress: String, isAction: Boolean, actionValue : String)
}