package com.shaadi.assingment.data.network

import com.google.gson.GsonBuilder
import com.shaadi.assingment.data.network.response.ProfileResponse
import com.shaadi.assingment.data.network.response.ProfileResult
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


interface RestApiClient {

    @GET("/api/")
    suspend fun fetchProfile(
        @Query("results") results: String
    ): Response<ProfileResponse>

    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ): RestApiClient {

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .addInterceptor(logging)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://randomuser.me")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(RestApiClient::class.java)
        }
    }
}