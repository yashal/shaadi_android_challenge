package com.shaadi.assingment.data.repository

import androidx.lifecycle.MutableLiveData
import com.shaadi.assingment.data.db.AppDataBase
import com.shaadi.assingment.data.network.RestApiClient
import com.shaadi.assingment.data.network.SafeApiRequest
import com.shaadi.assingment.data.network.response.ProfileResponse
import com.shaadi.assingment.data.network.response.ProfileResult
import com.shaadi.assingment.utils.Coroutines
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MainRepository(
    private val api: RestApiClient,
    private val db: AppDataBase
) : SafeApiRequest() {

    private val quotes = MutableLiveData<List<ProfileResult>>()

    suspend fun getProfileResult(): ProfileResponse {
        val response =  apiRequest { api.fetchProfile(RESULT) }
        quotes.postValue(response.results)
        return response
    }

    init {
        quotes.observeForever {
            saveReposInDataBase(it)
        }
    }

    private fun saveReposInDataBase(arrList: List<ProfileResult>){
        if (!arrList.isNullOrEmpty()){
            Coroutines.io{
                db.getProfileListDao().saveProfileList(arrList)
            }
        }
    }

    suspend fun getReposFromDataBase(): List<ProfileResult> {
        return withContext(Dispatchers.IO){
            db.getProfileListDao().getProfileList()
        }
    }

    fun updateValuesInDatabase(emailAddress: String, isAction: Boolean, actionValue : String){
        if (!emailAddress.isNullOrEmpty()){
            Coroutines.io{
                db.getProfileListDao().updateProfile(emailAddress, isAction, actionValue)
            }
        }
    }
    companion object {
        const val RESULT = "10"
    }
}