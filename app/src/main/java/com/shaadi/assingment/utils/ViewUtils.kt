package com.shaadi.assingment.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


enum class DateFormat {
    USER_READABLE_WITH_TIME, TWENTY_FOUR_HOURS
}

fun getSimpleDateFormat(dateFormat: DateFormat): SimpleDateFormat? {
    var simpleDateFormat: SimpleDateFormat? = null
    if (dateFormat == DateFormat.USER_READABLE_WITH_TIME) {
        simpleDateFormat =
            SimpleDateFormat("EEE, dd MMM", Locale.ENGLISH)
    } else if (dateFormat == DateFormat.TWENTY_FOUR_HOURS) {
        simpleDateFormat =
            SimpleDateFormat(
                "HH:mm",
                Locale.ENGLISH
            )
    }
    return simpleDateFormat
}

fun getCurrentTime(): Long{
    val currentEpochTIme = System.currentTimeMillis()
    return currentEpochTIme / 1000
}

 fun changeDateFormat(currentFormat: String, requiredFormat: String, dateString: String): String? {
    var result = ""
    val formatterOld =
        SimpleDateFormat(currentFormat, Locale.getDefault())
    val formatterNew =
        SimpleDateFormat(requiredFormat, Locale.getDefault())
    var date: Date? = null
    try {
        date = formatterOld.parse(dateString)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    if (date != null) {
        result = formatterNew.format(date)
    }
    return result
}
