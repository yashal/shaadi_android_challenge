package com.shaadi.assingment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.shaadi.assingment.data.db.AppDataBase
import com.shaadi.assingment.data.db.ProfileListDao
import com.shaadi.assingment.data.network.RestApiClient
import com.shaadi.assingment.data.network.SafeApiRequest
import com.shaadi.assingment.data.network.response.ProfileResponse
import com.shaadi.assingment.data.network.response.ProfileResult
import com.shaadi.assingment.data.repository.MainRepository
import com.shaadi.assingment.data.repository.MainRepository.Companion.RESULT
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MainRepositoryTest {

    @Mock
    private lateinit var myAPi: RestApiClient

    @Mock
    private lateinit var db: AppDataBase

    @Mock
    private lateinit var dao: ProfileListDao

    @Mock
    private lateinit var safeApiRequest: SafeApiRequest

    // Instant task Rule executor for Live data Mocking
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    // Dispatcher & Scope for testing coroutines
    val testDispatcher = TestCoroutineDispatcher()
    val testScope = TestCoroutineScope(testDispatcher)

    private lateinit var mainRepository: MainRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        mainRepository = MainRepository(myAPi, db)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }
    @Test
    fun `main repo executes api call`(){
        testScope.runBlockingTest {

            //Given
            val profileResponse = Mockito.mock(ProfileResponse::class.java)
            whenever( safeApiRequest.apiRequest { myAPi.fetchProfile(RESULT)}).thenReturn(profileResponse)

            //When
            myAPi.fetchProfile(RESULT)

            //Then
            verify(myAPi, times(1)).fetchProfile(RESULT)
            verifyNoMoreInteractions(myAPi)
        }
    }

    @Test
    fun `main repo save Data in local DB`() {
        val profileResult =  Mockito.mock(ProfileResult::class.java)
        val profileList = listOf(profileResult)
        Mockito.doNothing().`when`(dao).saveProfileList(profileList)

        verify(dao, times(0)).saveProfileList(profileList)
        verifyNoMoreInteractions(dao)
    }

    @Test
    fun `main repo get Data from local DB`() {
        //Given
        val profileResult =  Mockito.mock(ProfileResult::class.java)
        val profileList = listOf(profileResult)
        whenever(dao.getProfileList()).thenReturn(profileList)

        //then
        verify(dao, times(0)).getProfileList()
        verifyNoMoreInteractions(dao)
    }

    @Test
    fun `main repo update Data in local DB`() {
        Mockito.doNothing().`when`(dao).updateProfile("yashpremsharma@gmail.com", true, "Member Accepted")

        verify(dao, times(0)).updateProfile("yashpremsharma@gmail.com", true, "Member Accepted")
        verifyNoMoreInteractions(dao)
    }

}