package com.shaadi.assingment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.shaadi.assingment.data.network.response.ProfileResponse
import com.shaadi.assingment.data.network.response.ProfileResult
import com.shaadi.assingment.data.repository.MainRepository
import com.shaadi.assingment.ui.viewmodel.MainViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MainViewModelTest {

    // Mock the repository object and verify interactions on this mock
    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    // Instant task Rule executor for Live data Mocking
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    // Dispatcher & Scope for testing coroutines
    val testDispatcher = TestCoroutineDispatcher()
    val testScope = TestCoroutineScope(testDispatcher)

    private lateinit var mainViewModel: MainViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(testDispatcher)
        mainViewModel = MainViewModel(mainRepository)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testScope.cleanupTestCoroutines()
    }

    @Test
    fun `main view model executes apiCall`() {
        testScope.runBlockingTest {

            val profileResponse = Mockito.mock(ProfileResponse::class.java)
            whenever(mainRepository.getProfileResult()).thenReturn(profileResponse)

            verify(mainRepository, times(0)).getProfileResult()
            verifyNoMoreInteractions(mainRepository)
        }
    }

    @Test
    fun `main view model executes DBCall`() {
        testScope.runBlockingTest {

            val profileResponse = Mockito.mock(ProfileResult::class.java)
            val profileList = listOf(profileResponse)
            whenever(mainRepository.getReposFromDataBase()).thenReturn(profileList)

            verify(mainRepository, times(0)).getReposFromDataBase()
            verifyNoMoreInteractions(mainRepository)

        }
    }
}