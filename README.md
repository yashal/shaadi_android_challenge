# Shadi.com Android Assignment Profile Matches Project
======================================================

This is the application for Android Assignment Profile Matches Projects.
First we call the API form android method. 
Here first we check is any data is stored in local database or not.
For first time we don't have any data in database then we call the API and also check the internet check validations.

*We can provide offline support by two ways:
A). Save the data from the api call and show the data from the database and their response (accept and decline ) when the internet is not available.
B). Save the data form api and show the data from the database when and their response (accept and decline) next time the app opens i.e. no api call for next time.
the first approach is not possible every time when we open the app, the new data is fetched from the API. In this case when we save the data and next time open the app and new data will be replaced with that data.
Here I put second Logic because we have to provide offline support and every time api call data  will be changed.

The API url is:
https://randomuser.me/api/?results=10

If api got any exception then it shows the error screen at this screen there is a CTA "Retry", by clicking this CTA we can again call the API.
If we don't found any profile then simple show the error text message saying "No Profile Available"

First time this api is called shows the result in  list and the same time we save the result in the local database.
We are showing the following data in list:
*Image
*Name
*Gender
*age
*Address (including city, state and country)
If next time when user visits or if there is internet connection is not there then we show the data from local database.
There are two buttons.
1. Accept (In green Color)
2. Reject (In red Color)
By clicking Accept button we will show the text "Member Accepted" and buttons are not longer available there
By clicking Reject button we will show the text "Member Declined" and buttons are not longer available there
by the same time we will save each status in local database and if user open again the same status will be shown.

* In this project we are using the following technologies:
Language: Kotlin
Architecture: MVVM (Model View View Model)
Design Pattern: Kodein, Coroutine
Design Library: Material Design
Reactive Programing: LiveData
API Call: Retrofit and GSON
For API Loading: Shimmer Effect
MinimumSDK version: 19
Local Database: Room database

*It also covers JUnit Test Cases